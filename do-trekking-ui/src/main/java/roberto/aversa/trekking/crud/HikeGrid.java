package roberto.aversa.trekking.crud;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import roberto.aversa.trekking.backend.data.Category;
import roberto.aversa.trekking.backend.data.Hike;
import roberto.aversa.trekking.backend.data.Product;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class HikeGrid extends Grid<Hike> {

    public HikeGrid() {
        setSizeFull();

        addColumn(Hike::getPlace)
                .setHeader("Place")
                .setFlexGrow(20)
                .setSortable(true);

        // Format and add " €" to price
        final DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setMinimumFractionDigits(2);


        // To change the text alignment of the column, a template is used.
        final String placeTemplate = "<div style='text-align: right'>[[item.place]]</div>";
        addColumn(TemplateRenderer.<Hike>of(placeTemplate)
                .withProperty("place", hike -> hike.getPlace()))
                .setHeader("Place")
                .setComparator(Comparator.comparing(Hike::getPrice))
                .setFlexGrow(3);


        // To change the text alignment of the column, a template is used.
        final String dayTemplate = "<div style='text-align: right'>[[item.day]]</div>";
        addColumn(TemplateRenderer.<Hike>of(placeTemplate)
                .withProperty("day", hike -> hike.getDay()))
                .setHeader("Day")
                .setComparator(Comparator.comparing(Hike::getDay))
                .setFlexGrow(3);

        // To change the text alignment of the column, a template is used.
        final String priceTemplate = "<div style='text-align: right'>[[item.price]]</div>";
        addColumn(TemplateRenderer.<Hike>of(priceTemplate)
                .withProperty("price", product -> decimalFormat.format(product.getPrice().getAmount().doubleValue()) + " €"))
                .setHeader("Price")
                .setComparator(Comparator.comparing(Hike::getPrice))
                .setFlexGrow(3);

        // Add an traffic light icon in front of availability
        // Three css classes with the same names of three availability values,
        // Available, Coming and Discontinued, are defined in shared-styles.css and are
        // used here in availabilityTemplate.
        final String availabilityTemplate = "<iron-icon icon=\"vaadin:circle\" class-name=\"[[item.difficulty]]\"></iron-icon> [[item.availability]]";
        addColumn(TemplateRenderer.<Hike>of(availabilityTemplate)
                .withProperty("Difficulty", hike -> hike.getDifficulty().toString()))
                .setHeader("Difficulty")
                .setComparator(Comparator.comparing(Hike::getDifficulty))
                .setFlexGrow(5);


    }

    public Hike getSelectedRow() {
        return asSingleSelect().getValue();
    }

    public void refresh(Hike hike) {
        getDataCommunicator().refresh(hike);
    }


}
