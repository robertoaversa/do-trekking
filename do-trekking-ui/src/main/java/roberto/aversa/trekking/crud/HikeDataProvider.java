package roberto.aversa.trekking.crud;

import com.vaadin.flow.data.provider.ListDataProvider;
import roberto.aversa.trekking.backend.HikeRepository;
import roberto.aversa.trekking.backend.data.Hike;
import roberto.aversa.trekking.backend.data.Product;

import java.util.Locale;
import java.util.Objects;

public class HikeDataProvider extends ListDataProvider<Hike> {

    /** Text filter that can be changed separately. */
    private String filterText = "";

    public HikeDataProvider() { super(HikeRepository.get().getAllHikes()); }

    /**
     * Store given product to the backing data service.
     * 
     * @param hike
     *            the updated or new product
     */
    public void save(Hike hike) {

    }

    /**
     * Delete given product from the backing data service.
     * 
     * @param product
     *            the product to be deleted
     */
    public void delete(Product product) {
        refreshAll();
    }

    /**
     * Sets the filter to use for this data provider and refreshes data.
     * <p>
     * Filter is compared for product name, availability and category.
     * 
     * @param filterText
     *            the text to filter by, never null
     */
    public void setFilter(String filterText) {
        Objects.requireNonNull(filterText, "Filter text cannot be null.");
        if (Objects.equals(this.filterText, filterText.trim())) {
            return;
        }
        this.filterText = filterText.trim();

        setFilter(hike -> passesFilter(hike.getPlace(), filterText)
                || passesFilter(hike.getDifficulty(), filterText));
    }

    @Override
    public Integer getId(Hike hike) {
        Objects.requireNonNull(hike,
                "Cannot provide an id for a null hike.");
        return hike.getId();
    }

    private boolean passesFilter(Object object, String filterText) {
        return object != null && object.toString().toLowerCase(Locale.ENGLISH)
                .contains(filterText);
    }
}
