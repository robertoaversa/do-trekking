package roberto.aversa.trekking.backend.data;

import org.joda.money.Money;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

public class Hike{
    public enum Difficulty{
        E("Escursionisti"),
        T("Turistico"),
        EE("Escursionisti-Esperti");

        private final String name;

        private Difficulty(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
    private Integer id;
    private LocalDate day;
    private LocalTime start;
    private LocalTime end;
    private String place;
    private String description;
    private Difficulty difficulty;
    private Money price;

    private Hike(LocalDate day, LocalTime start, LocalTime end, String place, String description, Difficulty difficulty, Money price) {
        this.day = day;
        this.start = start;
        this.end = end;
        this.place = place;
        this.description = description;
        this.difficulty = difficulty;
        this.price = price;
        this.setId(new Random().nextInt());
    }
    static public HikeBuilder createHikeBuilder(){
        return new HikeBuilder();
    }

    public LocalDate getDay() {
        return day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }


    public static class HikeBuilder{

        private LocalDate day;
        private LocalTime start;
        private LocalTime end;
        private String place;
        private String description;
        private Difficulty difficulty;
        private Money price;

        public Hike build(){
            return new Hike(day,start,end,place,description,difficulty,price);
        }
        static public HikeBuilder createHikeBuilder(){
            return new HikeBuilder();
        }

        public HikeBuilder setDay(LocalDate day) {
            this.day = day;
            return this;
        }

        public HikeBuilder setStart(LocalTime start) {
            this.start = start;
            return this;
        }

        public HikeBuilder setEnd(LocalTime end) {
            this.end = end;
            return this;
        }

        public HikeBuilder setPlace(String place) {
            this.place = place;
            return this;
        }

        public HikeBuilder setDescription(String description) {
            this.description = description;
            return this;
        }

        public HikeBuilder setDifficulty(Difficulty difficulty) {
            this.difficulty = difficulty;
            return this;
        }

        public HikeBuilder setPrice(Money price) {
            this.price = price;
            return this;
        }


    }
}
