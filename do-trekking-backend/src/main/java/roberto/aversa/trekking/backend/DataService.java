package roberto.aversa.trekking.backend;

import java.io.Serializable;
import java.util.Collection;

import roberto.aversa.trekking.backend.data.Category;
import roberto.aversa.trekking.backend.data.Product;
import roberto.aversa.trekking.backend.mock.MockDataService;

/**
 * Back-end service interface for retrieving and updating product data.
 */
public abstract class DataService implements Serializable {

    public abstract Collection<Product> getAllProducts();

    public abstract Collection<Category> getAllCategories();

    public abstract void updateProduct(Product p);

    public abstract void deleteProduct(int productId);

    public abstract Product getProductById(int productId);

    public static DataService get() {
        return MockDataService.getInstance();
    }

}
