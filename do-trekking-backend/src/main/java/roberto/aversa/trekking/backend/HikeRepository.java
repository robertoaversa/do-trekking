package roberto.aversa.trekking.backend;

import roberto.aversa.trekking.backend.data.Hike;
import roberto.aversa.trekking.backend.mock.MockDataService;

import java.io.Serializable;
import java.util.List;

public interface HikeRepository extends Serializable {
    public abstract List<Hike> getAllHikes();

     static public HikeRepository get() {
        return MockDataService.getHikeRepository();
    }

    Hike getHikeById(int hikeId);
}
